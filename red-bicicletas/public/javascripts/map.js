var map = L.map('main_map').setView([3.43722, -76.5225], 13);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    
}).addTo(map);
  
//L.marker([3.435, -76.51]).addTo(map);

$.ajax({
     dataType: "json",
     url: "api/bicicletas",
     success: function(result)
     {
        console.log('Que');

        console.log(result);

        result.bicicletas.forEach( function (bici)
        {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
            
        
     }
})